package com.preferendum.utils

object Constant {
    const val DEVICEID = "deviceid"
    const val USERID="userid"
    const val FCMTOKEN ="fcm_token"
}