package com.preferendum.model

import com.google.gson.annotations.SerializedName



class GeniricModal {
    @SerializedName("code")
    var code: String? = null

    @SerializedName("message")
    var message: String? = null

    @SerializedName("status")
    var status: String? = null

}