package com.preferendum.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


 open class BaseModal :Serializable{
    @SerializedName("code")
    var code: String? = null

    @SerializedName("error")
    var error: String? = null

    @SerializedName("status")
    var status: String? = null
}