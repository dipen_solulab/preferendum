package com.preferendum.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PhoneVerification :BaseModal(){

    @SerializedName("data")
    var data: Data? = null

    inner class Data :Serializable{
        @SerializedName("created_at")
        var createdAt: String? = null

        @SerializedName("device_id")
        var deviceId: String? = null

        @SerializedName("fcm_token")
        var fcmToken: String? = null

        @SerializedName("id")
        var id: Long? = null

        @SerializedName("mobile")
        var mobile: String? = null

        @SerializedName("otp")
        var otp: Long? = null

        @SerializedName("updated_at")
        var updatedAt: String? = null

    }
}