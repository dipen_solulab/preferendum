package com.preferendum.view

import android.app.Application
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication

class BaseApplication :Application() {

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
    }
}