package com.preferendum.view.auth

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.contestee.extention.getValue
import com.contestee.extention.showAlert
import com.contestee.network.addTo
import com.mukesh.OnOtpCompletionListener
import com.oeye.extentions.goToActivity
import com.oeye.network.CallbackObserver
import com.oeye.network.Networking
import com.preferendum.R
import com.preferendum.firebase.MyFirebaseService
import com.preferendum.model.GeniricModal
import com.preferendum.model.PhoneVerification
import com.preferendum.utils.Constant
import com.preferendum.view.BaseActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_mobile_get.*
import kotlinx.android.synthetic.main.activity_otp_verification.*


class OtpVerificationActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_verification)
         val otp = intent.getStringExtra("otp");
        txtOtp.text = otp
        otp_view.setOtpCompletionListener( { otp ->
            Log.d("onOtpCompleted=>", otp)
            verifiedOtp(otp)
        })

        txtresend.setOnClickListener { resendOtp() }
    }

    public fun verifiedOtp(otp:String) {
        showProgressbar()
        val params = HashMap<String, Any>()
        params["mobile"] = intent.getStringExtra("phone")
        params["otp"] = otp
        Networking
            .with(this)
            .getServices()
            .verifyOtp(Networking.wrapParams(params))//wrapParams Wraps parameters in to Request body Json format
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : CallbackObserver<PhoneVerification>() {
                override fun onSuccess(response: PhoneVerification) {
                    val data = response
                    hideProgressbar()
                    if (data != null) {
                        // session = data
                      //  session.storeDataByKey(Constant.USERID,response.)
                        val i = Intent(this@OtpVerificationActivity, CreatePINActivity::class.java)
                    //    i.putExtra("otp", response.data?.otp.toString())
                        startActivity(i)
                        finish()
                        //goToActivityAndClearTask<OtpVerificationActivity>(b)
                    } else {
                        showAlert(getString(R.string.something_went_wrong))
                    }
                }

                override fun onFailed(code: Int, message: String) {
                    showAlert(message)
                    hideProgressbar()
                }

            }).addTo(autoDisposable)
    }

    public fun resendOtp() {
        showProgressbar()

        val params = HashMap<String, Any>()
        params["mobile"] = intent.getStringExtra("phone")
        Networking
            .with(this)
            .getServices()
            .resendOtp(Networking.wrapParams(params))//wrapParams Wraps parameters in to Request body Json format
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : CallbackObserver<PhoneVerification>() {
                override fun onSuccess(response: PhoneVerification) {
                    val data = response
                    hideProgressbar()
                    if (data != null) {
                       otp_view.text?.clear()
                    } else {
                        showAlert(getString(R.string.something_went_wrong))
                    }
                }

                override fun onFailed(code: Int, message: String) {
                    showAlert(message)
                    hideProgressbar()
                }

            }).addTo(autoDisposable)
    }
}
