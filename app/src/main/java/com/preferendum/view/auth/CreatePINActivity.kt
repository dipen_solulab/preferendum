package com.preferendum.view.auth

import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.preferendum.R
import kotlinx.android.synthetic.main.activity_create_pin.*

class CreatePINActivity :AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_pin)
        pvNew.requestPinEntryFocus()
        pvNew.setPinViewEventListener { pinview, fromUser ->
            llNewPIN.visibility = View.GONE
            llConfirmPIN.visibility = View.VISIBLE
            pvConfirm.requestPinEntryFocus()
        }

        pvConfirm.setPinViewEventListener { pinview, fromUser ->
            if (pvNew.value == pinview.value) {
                //session.storeDataByKey(Constants.PIN, pinview.value)
                //session.isPinEnable = true
                setResult(Activity.RESULT_OK)
                finish()
            } else {
                //showToastShort("PIN does not match! try again")
                //mBinder.pvConfirm.requestPinEntryFocus()
                //mBinder.pvConfirm.setValue("")
            }
        }
    }
}