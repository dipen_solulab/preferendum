package com.preferendum.view.auth

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.telephony.TelephonyManager
import android.util.Log
import android.util.Log.e
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.contestee.extention.getValue
import com.contestee.extention.isEmpty
import com.contestee.extention.showAlert
import com.contestee.extention.showSnackBar
import com.contestee.network.addTo
import com.github.florent37.runtimepermission.kotlin.askPermission
import com.google.android.datatransport.runtime.logging.Logging.e
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.oeye.extentions.*
import com.oeye.network.CallbackObserver
import com.oeye.network.Networking
import com.preferendum.R
import com.preferendum.model.PhoneVerification
import com.preferendum.utils.Constant
import com.preferendum.view.BaseActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_mobile_get.*
import java.util.logging.Logger


class SendOtpActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mobile_get)
        Log.d("code", countryCodePicker.selectedCountryCode)
        btnSendOtp.setOnClickListener {
            checkPermission()

        }
       /* if (!session.contains(Constant.DEVICEID)) {
            checkPermission()
        }*/
    }

    public fun validate() {
        if (editText.isEmpty()) {
            root.showSnackBar("Please Enter Mobile Number")
        } else if (editText.getValue().length < 5 || editText.getValue().length > 15) {
            root.showSnackBar("Please enter valid mobile number. Mobile number should be contain min 5 to 15 numbers long ")
        } else {

            //FCM Token Logic
            // showProgressbar()
            FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        // Logger.e("getInstanceId failed", task.exception?.message.toString())
                        //showAlert("Something went wrong please restart Application!")
                        //hideProgressbar()
                        return@OnCompleteListener
                    }
                    // Get new Instance ID token
                    val token = task.result?.token
                    sendOtp(token)
                })
        }
    }

    public fun sendOtp(toke: String?) {
        showProgressbar()
        val params = HashMap<String, Any>()
        params["mobile"] = "+" + countryCodePicker.selectedCountryCode + editText.getValue()
        params["device_id"] = session.getDataByKey(Constant.DEVICEID)
        params["fcm_token"] = toke.toString()
        Networking
            .with(this)
            .getServices()
            .sendOtp(Networking.wrapParams(params))//wrapParams Wraps parameters in to Request body Json format
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : CallbackObserver<PhoneVerification>() {
                override fun onSuccess(response: PhoneVerification) {
                    val data = response.data
                    hideProgressbar()
                    if (data != null) {
                        // session = data

                        session.storeDataByKey(
                            Constant.FCMTOKEN,
                            response.data?.fcmToken.toString()
                        )
                        val i = Intent(this@SendOtpActivity, OtpVerificationActivity::class.java)
                        i.putExtra("otp", response.data?.otp.toString())
                        i.putExtra(
                            "phone",
                            "+" + countryCodePicker.selectedCountryCode + editText.getValue()
                        )
                        startActivity(i)
                        finish()
                        //goToActivityAndClearTask<OtpVerificationActivity>(b)
                    } else {
                        showAlert(getString(R.string.something_went_wrong))
                    }
                }

                override fun onFailed(code: Int, message: String) {
                    showAlert(message)
                    hideProgressbar()
                }

            }).addTo(autoDisposable)
    }

    private fun getData() {
        getDeviceID()


    }

    fun checkPermission() {
        askPermission(Manifest.permission.READ_PHONE_STATE) {
            //all permissions already granted or just granted
            getData()

        }.onDeclined { e ->
            if (e.hasDenied()) {

                AlertDialog.Builder(this).setMessage("Please accept our permissions")
                    .setPositiveButton("yes") { dialog, which ->
                        e.askAgain();
                    } //ask again
                    .show();
            }

            if (e.hasForeverDenied()) {
                AlertDialog.Builder(this).setMessage("Please accept our permissions")
                    .setPositiveButton("yes") { dialog, which ->
                        e.goToSettings()
                    } //ask again
                    .show();
            }
        }
    }


    fun getDeviceID() {
        val telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
            == PackageManager.PERMISSION_GRANTED
        ) {
            // Permission is  granted
            val deviceID: String? = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
            } else { // older OS  versions
                telephonyManager.deviceId
            }

            deviceID?.let {
                session.storeDataByKey(
                    Constant.DEVICEID,
                    deviceID
                )

                validate()
                Log.i("Log", "DeviceId=$deviceID")
            }

        } else {  // Permission is not granted
            checkPermission()
        }
    }
}

