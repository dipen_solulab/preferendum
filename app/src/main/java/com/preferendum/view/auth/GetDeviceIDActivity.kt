package com.preferendum.view.auth

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.telephony.TelephonyManager
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat

import com.github.florent37.runtimepermission.kotlin.askPermission
import com.preferendum.R
import com.preferendum.firebase.MyFirebaseService
import kotlinx.android.synthetic.main.activity_device_id.*
import java.util.*
import kotlin.collections.HashMap

class GetDeviceIDActivity: AppCompatActivity() {
    private var uniqueID: String? = null
    private val PREF_UNIQUE_ID = "PREF_UNIQUE_ID"
    var deviceId = ""
    var deviceUUID = ""
    var deviceImei = ""
    lateinit var context: Context
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_device_id)
        context = this
      //  checkPermission()
        /*txtIMEI1.text = deviceUUID
        deviceUUID
        deviceImei
        txtDeviceID.text=  deviceId*/


    }



    private fun getData() {
        deviceUUID = getUUID(context)!!
        getDeviceID()
        try {

            getIMEI()
        } catch (e: SecurityException) {
            println("SecurityException ===========")
            e.printStackTrace()
        }

    }

    fun checkPermission() {
        askPermission(Manifest.permission.READ_PHONE_STATE) {
            //all permissions already granted or just granted
            getData()

        }.onDeclined { e ->
            if (e.hasDenied()) {

                AlertDialog.Builder(this).setMessage("Please accept our permissions")
                    .setPositiveButton("yes") { dialog, which ->
                        e.askAgain();
                    } //ask again
                    .setNegativeButton("no") { dialog, which ->
                        dialog.dismiss();
                    }
                    .show();
            }

            if (e.hasForeverDenied()) {
                // you need to open setting manually if you really need it
                e.goToSettings();
            }
        }
    }


    fun getDeviceID() {
        val telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
            == PackageManager.PERMISSION_GRANTED
        ) {
            // Permission is  granted
            val deviceID: String? = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
            } else { // older OS  versions
                telephonyManager.deviceId
            }

            deviceID?.let {
                deviceId = deviceID
                Log.i("Log", "DeviceId=$deviceID")
            }

        } else {  // Permission is not granted

        }
    }


    fun getIMEI() {
        var imei: String? = null
        val mTelephony = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
            == PackageManager.PERMISSION_GRANTED
        ) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                imei = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    if (mTelephony.phoneType == TelephonyManager.PHONE_TYPE_GSM) {
                        if (mTelephony.phoneCount == 2) {
                            mTelephony.getImei(0)
                        } else {
                            mTelephony.imei
                        }
                    } else {
                        if (mTelephony.phoneCount == 2) {
                            mTelephony.getMeid(0)
                        } else {
                            mTelephony.meid
                        }
                    }

                } else {
                    if (mTelephony.phoneCount == 2) {
                        mTelephony.getDeviceId(0)
                    } else {
                        mTelephony.deviceId
                    }
                }
            } else {
                imei = mTelephony.deviceId
            }
            imei?.let {
                deviceImei = imei
                Log.i("Log", "imei=$imei")
            }
        } else {  // Permission is not granted

        }
    }

    @Synchronized
    fun getUUID(context: Context): String? {
        if (uniqueID == null) {
            val sharedPrefs = context.getSharedPreferences(
                PREF_UNIQUE_ID, Context.MODE_PRIVATE
            )
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null)
            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString()
                val editor = sharedPrefs.edit()
                editor.putString(PREF_UNIQUE_ID, uniqueID)
                editor.apply()
            }
        }
        uniqueID?.let {
            Log.i("Log", "uniqueID=$uniqueID")
        }
        return uniqueID
    }


}














