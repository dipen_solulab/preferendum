package com.preferendum.view

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.provider.Settings
import android.telephony.TelephonyManager
import android.util.Log
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.github.florent37.runtimepermission.kotlin.askPermission
import com.preferendum.R
import com.preferendum.utils.Constant
import com.preferendum.view.auth.OtpVerificationActivity
import com.preferendum.view.auth.SendOtpActivity

class SplashActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_spash_screen)



        Handler().postDelayed({
            if (session.isLoggedIn) {
                val i = Intent(this, SendOtpActivity::class.java)
                startActivity(i)
                finish()
            } else {
                val i = Intent(this, SendOtpActivity::class.java)
                startActivity(i)
                finish()
            }

        }, 1000)
    }



}