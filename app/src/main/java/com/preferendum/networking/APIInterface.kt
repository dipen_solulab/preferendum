package com.oeye.network


import com.preferendum.model.GeniricModal
import com.preferendum.model.PhoneVerification
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST


interface APIInterface {
    @POST("sendOtp")
    fun sendOtp(@Body body: RequestBody): Observable<Response<PhoneVerification>>

    @POST("verifyOtp")
    fun verifyOtp(@Body body: RequestBody): Observable<Response<PhoneVerification>>

    @POST("resendOtp")
    fun resendOtp(@Body body: RequestBody): Observable<Response<PhoneVerification>>
}
