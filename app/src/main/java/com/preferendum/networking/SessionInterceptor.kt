package com.oeye.network

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class SessionInterceptor() : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()

        val builder = original.newBuilder()
     //   builder.header("Authorization", "Bearer r3jmqw11ujrmt6wdg21arj2acq9hhsa1")
        builder.method(original.method, original.body)
        return chain.proceed(builder.build())
    }
}