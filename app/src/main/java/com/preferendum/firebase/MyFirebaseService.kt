package com.preferendum.firebase

import android.content.Context
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage




class MyFirebaseService : FirebaseMessagingService() {


  override  fun onNewToken(s: String) {
        super.onNewToken(s!!)
        Log.e("newToken", s)
        getSharedPreferences("_", Context.MODE_PRIVATE).edit().putString("token", s)
            .apply()
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage!!)
    }

   companion object{
       fun getToken(context: Context): String? {
           return context.getSharedPreferences("_", Context.MODE_PRIVATE)
               .getString("token", "empty")
       }
   }
}